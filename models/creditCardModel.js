const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        type: { type: DataTypes.STRING, allowNull: false },
        number: { type: DataTypes.STRING, allowNull: false },
        securityCode: { type: DataTypes.STRING, allowNull: false },
        expirationDate: { type: DataTypes.STRING, allowNull: false },
        monthlyLimit: { type: DataTypes.FLOAT, allowNull: false }
    };

    return sequelize.define('creditCard', attributes);
}