# cinnamon-api


Node.js v14.16 + MySQL API, Authentication and Registration for users. Childs and credit cards

Need to change values of config/db.json with your local MySQL data connection

npm install

To start app need to run the command npm start and if the set up of database is correct the database will create with the tables and the relationships between them. Also will create one user

user jose.sosa@cinnamon.agency
password test1234

+----------------------------------+
| Tables_in_test_child_credit_card |
+----------------------------------+
| children                         |
| creditCards                      |
| users                            |
+----------------------------------+

Endpoints

/users ->get
/users/signUp ->post
body: name, email, password
/users/signIn ->post
body: name, password

/childs ->get
/childs/:id ->get With the relation with credit card
/childs/:id ->delete
/childs/:id ->put 
body: name, age
/childs/assignCreditCard/:id/card/:idCard ->put

/creditCards/ ->post
body: type,securityCode,monthlyLimit,expirationDate
/creditCards/:id ->put 
body: monthlyLimit
/creditCards/charge/:id ->put 
body: amount

All endponts needs Authorization Bearer token the same is provides in singIn endpoint.

