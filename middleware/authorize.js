const jwt = require('express-jwt');
const { secret } = require('config/jwt.json');
const db = require('providers/db');

// receives a boolean parameter if authorization is administrator only
const authorize = () => {
    return [
        // token to request as req.user
        jwt({ secret, algorithms: ['HS256'] }),

        // attach full user record to request object
        async (req, res, next) => {
            // get user with id from token sub
            const user = await db.User.findByPk(req.user.sub);

            // check if user exists, also if the function receives requiresAdmin check if user is admin
            if (!user)
                return res.status(401).json({ message: 'Unauthorized' });

            // authorization successful
            req.user = user.get();
            next();
        }
    ];
}

module.exports = {
    authorize
}
