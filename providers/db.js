const configDb = require("config/db.json");
const mysql = require("mysql2/promise");
const { Sequelize } = require("sequelize");
const bcrypt = require("bcryptjs");

module.exports = db = {};

initialize();

async function initialize() {
  // create db if it doesn't already exist
  const { host, port, user, password, database } = configDb;
  const connection = await mysql.createConnection({
    host,
    port,
    user,
    password,
  });
  await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);

  // connect to db
  const sequelize = new Sequelize(database, user, password, {
    dialect: "mysql",
  });

  // init models and add them to the exported db object
  db.User = require("../models/userModel")(sequelize);
  db.Child = require("../models/childModel")(sequelize);
  db.CreditCard = require("../models/creditCardModel")(sequelize);

  // relationships

  db.User.hasMany(db.Child, {as: 'childs', onDelete: "cascade", hooks: true });
  db.Child.belongsTo(db.User);
  db.CreditCard.hasOne(db.Child, {as: 'creditCards'});
  db.Child.belongsTo(db.CreditCard)

  // sync all models with database
  await sequelize.sync();

  // create admin user with email set in config/email, password test1234 as example
  await db.User.findOrCreate({
    where: { email: "jose.sosa@cinnamon.agency" },
    defaults: {
      name: "jose",
      email: "jose.sosa@cinnamon.agency",
      password: await bcrypt.hash("test1234", 10),
    },
  });
}
