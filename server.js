﻿require('rootpath')();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const errorHandler = require('middleware/errorHandler');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// api routes
app.use('/users', require('./controllers/userController'));
app.use('/childs', require('./controllers/childController'));
app.use('/creditCards', require('./controllers/creditCardController'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4001;
app.listen(port, () => console.log('Server listening on port ' + port));