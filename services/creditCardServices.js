﻿const db = require("providers/db");

module.exports = {
  create,
  update,
  _delete,
  charge,
  getCreditCard,
};

async function create(params) {
  params.number = Math.floor(Math.random() * 1000000000000000) + 1;
  // save post
  await db.CreditCard.create(params);
}

async function update(id, params) {
  const creditCard = await getCreditCard(id);
  const { monthlyLimit } = params;
  // copy params to post and save
  Object.assign(creditCard, { monthlyLimit });
  await creditCard.save();
}

async function _delete(id) {
  const creditCard = await getCreditCard(id);
  await creditCard.destroy();
}

async function getCreditCard(id) {
  const creditCard = await db.CreditCard.findByPk(id);
  if (!creditCard) throw "Credit Card not found";
  return creditCard;
}

async function charge(id, params) {
    const creditCard = await getCreditCard(id);
    if (params.amount > creditCard.monthlyLimit) throw 'limit exceeded'
    const newLimit = creditCard.monthlyLimit - params.amount;
    Object.assign(creditCard, { monthlyLimit: newLimit });
    await creditCard.save();
  }