﻿const db = require('providers/db');

module.exports = {
    create,
    getAll,
    getById,
    update,
    assignCreditCard,
    delete: _delete
};

async function create(params, user) {
    params.userId = user.id;
    // save post
    await db.Child.create(params);
}

// receives id and find post by id, userId to show only to owners posts hidden
async function getById(id, userId) {

    // find all posts that are not hidden, but if you are the owner, show the post even though it is hidden 
    return await db.Child.findOne({
        where: {
            id
        },
        include: ['creditCard']
    });
}

// receives userId to show only to owners posts hidden, the number of page and the size of each page
async function getAll(userId) {
    return await db.Child.findAll({userId});
}

async function update(id, params) {
    const child = await getChild(id);

    // copy params to post and save
    Object.assign(child, params);
    await child.save();
}

async function assignCreditCard(id, idCard, userId) {
    const child = await getChild(id);
    if(userId != child.userId) throw 'The user authenticatd is not the parent';
    // copy params to post and save
    Object.assign(child, {creditCardId: idCard});
    await child.save();
}

async function _delete(id) {
    const child = await getChild(id);
    await child.destroy();
}

async function getChild(id) {
    const child = await db.Child.findByPk(id);
    if (!child) throw 'Child not found';
    return child;
}