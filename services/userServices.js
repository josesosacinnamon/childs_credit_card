﻿const { secret } = require('config/jwt.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('providers/db');
const { Op } = require("sequelize");

module.exports = {
    signIn,
    signUp,
    getAll,
    getById
};

async function signIn({ email, password }) {
    const user = await db.User.scope('withPassword').findOne({ where: { email } });

    if (!user || !(await bcrypt.compare(password, user.password)))
        throw 'Email or password is incorrect';

    // authentication successful
    const token = jwt.sign({ sub: user.id, admin: user.isAdmin }, secret, { expiresIn: '1d' });
    return { ...withoutPassword(user.get()), token };
}

async function signUp(params) {
    // validate
    if (await db.User.findOne({ where: { email: params.email } })) {
        throw 'Email "' + params.email + '" is already taken';
    }

    // hash password
    if (params.password) {
        params.password = await bcrypt.hash(params.password, 10);
    }

    // save user
    await db.User.create(params);
}

async function getAll() {
    return await db.User.findAll({include:['childs']});
}

async function getById(id) {
    return await getUser(id);
}

async function getUser(id) {
    const user = await db.User.findByPk(id);
    if (!user) throw 'User not found';
    return user;
}

// remove password to show user's information
function withoutPassword(user) {
    const { password, ...userWithoutPassword } = user;
    return userWithoutPassword;
}