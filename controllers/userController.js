﻿const express = require("express");
const router = express.Router();
const Joi = require("joi");
const validateRequest = require("middleware/validateRequest");
const { authorize } = require("middleware/authorize");
const userServices = require("services/userServices");

// routes
router.post("/signIn", signInSchema, signIn);
router.post("/signUp", signUpSchema, signUp);
router.get("/", authorize(), getAll);

module.exports = router;

function signInSchema(req, res, next) {
  const schema = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
  });
  validateRequest(req, next, schema);
}

function signIn(req, res, next) {
  userServices
    .signIn(req.body)
    .then((user) => res.json(user))
    .catch(next);
}

function signUpSchema(req, res, next) {
  const schema = Joi.object({
    firstName: Joi.string(),
    lastName: Joi.string(),
    email: Joi.string().required(),
    password: Joi.string().min(8).required(),
  });
  validateRequest(req, next, schema);
}

function signUp(req, res, next) {
  userServices
    .signUp(req.body)
    .then(() => res.json({ message: "Registration successful" }))
    .catch(next);
}

function getAll(req, res, next) {
  userServices
    .getAll()
    .then((users) => res.json(users))
    .catch(next);
}
