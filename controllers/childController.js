﻿const express = require("express");
const router = express.Router();
const Joi = require("joi");
const validateRequest = require("middleware/validateRequest");
const { authorize } = require("middleware/authorize");
const childServices = require("../services/childServices");

// routes
router.post("/", childSchema, authorize(), create);
router.get("/", authorize(), getAll);
router.get("/:id", authorize(), getById);
router.put("/:id", authorize(), updateSchema, update);
router.delete("/:id", authorize(), _delete);

router.put("/assignCreditCard/:id/card/:idCard", authorize(), assignCreditCard);

module.exports = router;

function childSchema(req, res, next) {
  const schema = Joi.object({
    name: Joi.string().required(),
    age: Joi.number().integer().required(),
  });
  validateRequest(req, next, schema);
}

function create(req, res, next) {
  childServices
    .create(req.body, req.user)
    .then(() => res.json({ message: "Child created successfully" }))
    .catch(next);
}

function getById(req, res, next) {
  childServices
    .getById(req.params.id, req.user.id)
    .then((childs) => res.json(childs))
    .catch(next);
}

function getAll(req, res, next) {
  childServices
    .getAll(req.user.id)
    .then((child) => res.json(child))
    .catch(next);
}

function updateSchema(req, res, next) {
  const schema = Joi.object({
    name: Joi.string().empty(""),
    age: Joi.number().integer().empty(""),
  });
  validateRequest(req, next, schema);
}

function update(req, res, next) {
  childServices
    .update(req.params.id, req.body)
    .then(() => res.json({ message: "Child updated successfully" }))
    .catch(next);
}

function _delete(req, res, next) {
  childServices
    .delete(req.params.id)
    .then(() => res.json({ message: "Child deleted successfully" }))
    .catch(next);
}

function assignCreditCard(req, res, next) {
  childServices
    .assignCreditCard(req.params.id, req.params.idCard, req.user.id)
    .then(() => res.json({ message: `Credit card assigned successfully for child ${req.params.id}` }))
    .catch(next);
}
