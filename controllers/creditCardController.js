﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('middleware/validateRequest');
const { authorize } = require('middleware/authorize')
const creditCardServices = require('../services/creditCardServices');

// routes
router.post('/', creditCardSchema, authorize(), create);
router.put('/:id', updateSchema, authorize(), update);
router.put('/charge/:id', chargeSchema, charge);

module.exports = router;

function creditCardSchema(req, res, next) {
    const schema = Joi.object({
        childId: Joi.number().min(1).required(),
        type: Joi.string().required(),
        securityCode: Joi.number().min(1).required(),
        expirationDate: Joi.string().required(),
        monthlyLimit: Joi.number().required()
        
    });
    validateRequest(req, next, schema);
}

function create(req, res, next) {
    creditCardServices.create(req.body)
        .then(() => res.json({ message: 'Credit card created successfully' }))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        monthlyLimit: Joi.number().required()
        
    });
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    creditCardServices.update(req.params.id, req.body)
        .then(() => res.json({ message: 'Monthly Limit of Credit card updated successfully' }))
        .catch(next);
}

function chargeSchema(req, res, next) {
    const schema = Joi.object({
        amount : Joi.number().required()
        
    });
    validateRequest(req, next, schema);
}

function charge(req, res, next) {
    creditCardServices.charge(req.params.id, req.body)
        .then(() => res.json({ message: 'Charge registered'}))
        .catch(next);
}